package com.bondar.parser;

import com.bondar.parser.exception.InvalidStringException;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThrows;

public class ParserTest {

    private static final String STRING = "(id,created,employee(id,firstname,employeeType(id), lastname),location)";
    private static final String INVALID_STRING = "(id,created,employee)id,firstname,employeeType(id), lastname),location)";

    private static final String INCORRECT_NUMBER_OF_BRACKETS = "Error! invalid format string";
    private static final String STRING_CAN_NOT_BE_NULL = "String can not be null";

    private static final List<String> list = Arrays.asList("id", "created", "employee", "-id", "-firstname", "-employeeType", "--id", "-lastname", "location");


    private Parser parser = new Parser();

    @Test
    public void shouldThrowExceptionIfStringNull() {

        Throwable thrown = assertThrows(InvalidStringException.class, () -> {
            parser.parse(null);
        });

        assertEquals(STRING_CAN_NOT_BE_NULL, thrown.getMessage());
    }


    @Test
    public void shouldThrowExceptionIfStringHasInvalidFormat() {

        Throwable thrown = assertThrows(InvalidStringException.class, () -> {
            parser.parse(INVALID_STRING);
        });

        assertEquals(INCORRECT_NUMBER_OF_BRACKETS, thrown.getMessage());
    }

    @Test
    public void shouldSortAndparseString() {
        List<String> result = parser.parse(STRING);

        assertEquals(list, result);
    }


}
