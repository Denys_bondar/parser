package com.bondar.parser;

import com.bondar.parser.exception.InvalidStringException;

import java.util.*;

public class Parser {

    private static final String STRING = "(id,created,employee(id,firstname,employeeType(id), lastname),location)";

    private static final String INCORRECT_NUMBER_OF_BRACKETS = "Error! invalid format string";
    private static final String STRING_CAN_NOT_BE_NULL = "String can not be null";

    private static final String SPLIT_PATTERN_BY_SYMBOL = "";

    private static final String LEFT_BRACKETS = "(";
    private static final String RIGHT_BRACKETS = ")";


    private static final String PREFIX = "-";
    private static final String COMMA = ",";


    public List<String> parse(final String string) {

        validate(string);


        return splitString(string);
    }

    private List<String> splitString(final String string) {

        List<String> list = new ArrayList<>();

        String[] split = string.split("\\(");

        for (int i = 1; i < split.length; i++) {
            int count = i;

            for (String word : split[i].split(COMMA)) {
                if (word.endsWith(RIGHT_BRACKETS)) {
                    String replace = word.replace(")", "");
                    addToList(list, count, replace.trim());
                    count--;
                } else {
                    addToList(list, count, word.trim());
                }
            }
        }

        return list;
    }


    private void addToList(final List<String> list, final int count, final String element) {
        String elementWithPrefix = element;

        if (count > 1) {
            elementWithPrefix = getPrefix(count) + element;
        }

        list.add(elementWithPrefix);
    }

    private String getPrefix(int count) {
        String result = "";
        while (count != 1) {
            result = result + PREFIX;//we dont need String builder
            count--;
        }
        return result;
    }


    private void validate(final String string) {
        if (string == null) {
            throw new InvalidStringException(STRING_CAN_NOT_BE_NULL);
        }

        int count = 0;

        final String[] symbols = string.split(SPLIT_PATTERN_BY_SYMBOL);

        for (String symbol : symbols) {
            if (symbol.equals(LEFT_BRACKETS)) {
                count++;
            }

            if (symbol.equals(RIGHT_BRACKETS)) {
                count--;
            }
        }

        if (count != 0) {
            throw new InvalidStringException(INCORRECT_NUMBER_OF_BRACKETS);
        }
    }
}
