package com.bondar.parser.exception;

public class InvalidStringException extends RuntimeException{


    public InvalidStringException(String message) {
        super(message);
    }
}
